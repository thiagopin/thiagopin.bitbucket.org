# Fase 01 - Relatórios de todas as execução do experimento

Os relatórios desta área refletem as 30 execuções de cada cenário. Os cenários
podem ser selecionados no painel abaixo e os gráficos serão preenchidos com as
informações da execução escolhida.

Dois gráficos serão preenchidos. O primeiro mostrando a evolução do fitness,
máximo de horas e média de fitness de toda a população. O segundo gráfico exibe
mais informações referentes a evolução da população, exibindo o fitness mínimo,
o primeiro quartil, a mediana, o terceiro quartil e o máximo da população em
determinado período.

O nome da instância representa o seu tamanho e as variações que foram submetidas
durante o processo. Por exemplo `i_25_10_employee_30_00` indica atributos
separados por `_`, onde:

* 25 - é o número de tarefas da instancia;
* 10 - 10% de dependencia entre as tarefas;
* employee - um novo empregado será adicionado durante o processo de otimização;
* 30 - 30% de reaproveitamento genético entre as execuções de cada execução;
* 00 - número da execução, esse número varia entre 00 e 29 (total de 30 execuções).
