(function($) {

    var markdown = $('#content')

    $.get(markdown.attr('file'), function( content ) {
        var md = window.markdownit();
        var result = md.render(content);
        markdown.html(result)
    })

})($)
