(function($) {
    function tseries(data, instance, yproperty) {
        return data.filter(function(e) {
            return e.id == instance
        }).map(function(el, idx) {
            return { x: parseInt(el['seconds']), y: parseFloat(el[yproperty]) }
        });
    }

    function mountChart(target, data, instance, yproperties) {
        mountChart(target, data, instance, yproperties, false)
    }

    function mountChart(target, data, instance, yproperties, stacked) {
        var mydata = yproperties.map(function(el, idx) {
            return { values: tseries(data, instance, el),
                     key: el }
        })

        nv.addGraph(function() {
          var chart = (stacked ? nv.models.stackedAreaChart() : nv.models.lineChart())
            .useInteractiveGuideline(true)

          chart.xAxis
            .axisLabel('Time (s)')
            .tickFormat(d3.format(',r'))

          chart.yAxis
            .axisLabel(yproperties[0])
            .tickFormat(d3.format(',.2f'))

          d3.select(target)
            .datum(mydata)
            .transition().duration(500)
            .call(chart)

          nv.utils.windowResize(chart.update);

          return chart;
      })
    }

    function populateSelect(data) {
        var instances = {}
        data.forEach(function(el, idx){ instances[el.id] = el.id })
        var select = $('#executions')

        for(var prop in instances) {
            select.append($('<option/>').val(prop).text(prop))
        }
        return select
    }

    var dataFile = $('#data-file').val()

    $.get(dataFile, function( plainData ) {
        var data = d3.csv.parse(plainData)
        var select = populateSelect(data)
        $(select).on('change', function(){
            // var type = $('input[name=chartType]:checked').val() == 'stacked'

            mountChart('#fitness-chart svg', data, $(this).val(), ['fitness', 'maxHours', 'mean'], false)
            mountChart('#pop-chart svg', data, $(this).val(), ['min', 'q1', 'median', 'q3', 'max'], true)
        })

        mountChart('#fitness-chart svg', data, data[0].id, ['fitness', 'maxHours', 'mean'], false)
        mountChart('#pop-chart svg', data, data[0].id, ['min', 'q1', 'median', 'q3', 'max'], true)
    })

})($)
