# Relatórios de execução do experimento

A execução das simulações do trabalho será dividida em duas fases. As duas fases consistem em executar diversas simulações de otimização em um cenário que apresenta mudanças ao longo do tempo.

A primeira fase consiste em apresentar os resultados obtidos fazendo uso de uma abordagem dinâmica e uma abordagem tradicional. O objetivo dessa fase é apresentar se a abordagem dinâmica apresenta um resultado considerado melhor que a abordagem tradicional.

A segunda fase consiste em apresentar variações nos cenários iniciais, aumentando o número de tarefas ou de empregados durante a execução e analisar qualitativamente como o método de otimização reage em relação as mudanças.

## Fase 01

Para apresentar o comparativo entre uma abordagem dinâmica e uma tradicional, o seguinte cenário será utilizado:

* 25 tarefas;
* 10% de dependência entre tarefas;
* Por 3 vezes 5 novas tarefas serão adicionadas a cada 30 segundos;
* Por 3 vezes 1 novos empregado será adicionado a cada 30 segundos;
* 0%, 30%, 60%, 90% e 100% de reaproveitamento genético entre cada nova mudança.

30 simulações serão executadas para cada abordagem. Cada simulação terá cerca de 90 segundos (30 segundos para cada um dos três incrementos de tarefas). Portanto o tempo total de execução desta fase será de 27000 segundos (90 x 30 x 5 x 2) para das duas abordagens. Logo 27000 segundos (450 minutos ou 7 horas e 30 minutos) serão utilizados.

## Fase 02

A análise qualitativa será feita usando um comparativo visual sobre a evolução no resultado de diversas execuções em cenários diferentes. Nesta fase as instâncias irão variar em tamanho, possuindo entre 25, 50 e 100 tarefas onde 10%, 25% e 50% das tarefas possuem precedencias entre si.

As execuções usarão o aproveitamento genetico que possuiu melhor desempenho na fase passada e desta vez irão passar por adição de tarefas, empregados e ambos. Assim como da vez passada cada execução irá ter um tempo de 90 segundos.

Por exemplo, uma determinada instância i_50_25 (com 50 tarefas e 25% de precedência) terá 30 execuções com adição de tarefas, 30 execuções com adição de empregados e 30 execuções com adição de tarefas e empregados.

O tempo estimado de execução desta fase é de 72900 segundos (90 x 30 x 3 x 9) ou seja, 1215 minutos (~21 horas).
